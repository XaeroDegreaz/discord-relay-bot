package com.dobydigital.discordrelaybot.discord;

import com.dobydigital.discordrelaybot.discord.authentication.HttpBasicConfiguration;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.requests.RestAction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import java.time.OffsetDateTime;
import java.util.Collections;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

@RunWith( SpringRunner.class )
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
@ActiveProfiles( "test" )
public class ApiControllerTest
{
    private static final String API_EXTERNAL_POST_MESSAGE = "/api/external/post-message";
    @MockBean
    private JDA jda;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private HttpBasicConfiguration httpBasicConfiguration;

    @Test
    public void postMessage_Happy() throws Exception
    {
        //# Given
        ApiMessage apiMessage = new ApiMessage( "Foo Username", 123L, "Foo Message", "Foo Channel", 456L, OffsetDateTime.now() );
        TextChannel textChannel = mock( TextChannel.class );
        given( textChannel.sendMessage( anyString() ) )
            .willReturn( mock( RestAction.class ) );
        given( jda.getTextChannelsByName( apiMessage.getChannel(), true ) )
            .willReturn( Collections.singletonList( textChannel ) );
        //# When
        ResponseEntity<Void> responseEntity = restTemplate.withBasicAuth( httpBasicConfiguration.getUsername(), httpBasicConfiguration.getPassword() )
            .postForEntity( API_EXTERNAL_POST_MESSAGE, apiMessage, Void.class );
        //# Then
        assertThat( responseEntity.getStatusCode().is2xxSuccessful() )
            .isEqualTo( true );
        then( textChannel )
            .should()
            .sendMessage( anyString() );
    }

    @Test
    public void postMessage_FailureNullUsername() throws Exception
    {
        //# Given
        ApiMessage apiMessage = new ApiMessage( null, 123L, "Foo Message", "Foo Channel", 456L, OffsetDateTime.now() );
        //# When
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity( API_EXTERNAL_POST_MESSAGE, apiMessage, Void.class );
        //# Then
        assertThat( responseEntity.getStatusCode().is4xxClientError() )
            .isEqualTo( true );
    }

    @Test
    public void postMessage_FailureNullMessage() throws Exception
    {
        //# Given
        ApiMessage apiMessage = new ApiMessage( "Foo Username", 123L, null, "Foo Channel", 456L, OffsetDateTime.now() );
        //# When
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity( API_EXTERNAL_POST_MESSAGE, apiMessage, Void.class );
        //# Then
        assertThat( responseEntity.getStatusCode().is4xxClientError() )
            .isEqualTo( true );
    }

    @Test
    public void postMessage_FailureNullChannel() throws Exception
    {
        //# Given
        ApiMessage apiMessage = new ApiMessage( "Foo Username", 123L, "Foo Message", null, 456L, OffsetDateTime.now() );
        //# When
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity( API_EXTERNAL_POST_MESSAGE, apiMessage, Void.class );
        //# Then
        assertThat( responseEntity.getStatusCode().is4xxClientError() )
            .isEqualTo( true );
    }
}