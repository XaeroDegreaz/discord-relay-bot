package com.dobydigital.discordrelaybot.discord;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.TextChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( "/api/external" )
public class ApiController
{
    private final static Logger log = LoggerFactory.getLogger( ApiController.class );
    private final JDA jda;

    ApiController( JDA jda )
    {
        this.jda = jda;
    }

    @ExceptionHandler
    @ResponseStatus( value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal server error. This has been logged." )
    public void exceptionHandler( Throwable e )
    {
        log.error( "exceptionHandler()", e );
    }

    @ExceptionHandler
    @ResponseStatus( code = HttpStatus.BAD_REQUEST, reason = "There was an error validating your payload." )
    public void exceptionHandler( MethodArgumentNotValidException e )
    {
        log.error( "exceptionHandler()", e );
    }

    @PostMapping( "/post-message" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public void postMessage( @Validated @RequestBody ApiMessage message )
    {
        log.debug( "postMessage() - message:{}" );
        TextChannel channel = jda.getTextChannelsByName( message.getChannel(), true )
            .stream()
            .findFirst()
            .get();
        channel.sendMessage( String.format( "[%s]: %s", message.getSender(), message.getMessage() ) )
            .queue();
        log.debug( "postMessage() - Complete." );
    }
}
