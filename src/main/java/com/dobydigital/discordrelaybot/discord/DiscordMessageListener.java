package com.dobydigital.discordrelaybot.discord;

import com.dobydigital.discordrelaybot.relay.ExternalApiConfiguration;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.AsyncRestTemplate;
import java.util.concurrent.ExecutionException;

@Service
public class DiscordMessageListener extends ListenerAdapter
{
    private final static Logger log = LoggerFactory.getLogger( DiscordMessageListener.class );
    private final AsyncRestTemplate restTemplate;
    private final ExternalApiConfiguration externalApiConfiguration;

    DiscordMessageListener( AsyncRestTemplate restTemplate, ExternalApiConfiguration externalApiConfiguration )
    {
        this.restTemplate = restTemplate;
        this.externalApiConfiguration = externalApiConfiguration;
    }

    @Override
    public void onMessageReceived( MessageReceivedEvent event )
    {
        User user = event.getAuthor();
        Message message = event.getMessage();
        log.info( "onMessageReceived() - user:{}, message:{}", user, message );
        if ( event.isFromType( ChannelType.TEXT ) )
        {
            String relayEndpoint = externalApiConfiguration.getRelayEndpoint();
            try
            {
                ApiMessage apiMessage = new ApiMessage( message );
                HttpEntity<ApiMessage> httpEntity = new HttpEntity<>( apiMessage, externalApiConfiguration.getHttpHeaders() );
                restTemplate.postForEntity( relayEndpoint, httpEntity, Void.class ).get();
            }
            catch ( InterruptedException | ExecutionException e )
            {
                log.warn( "onMessageReceived() - Error posting to external relayEndpoint:{}", relayEndpoint );
                log.error( "onMessageReceived()", e );
            }
        }
        log.debug( "onMessageReceived() - Complete." );
    }
}
