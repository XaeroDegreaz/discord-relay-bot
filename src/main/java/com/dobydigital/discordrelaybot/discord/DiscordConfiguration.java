package com.dobydigital.discordrelaybot.discord;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotNull;

@Configuration
@ConfigurationProperties( "discord-configuration" )
@Validated
public class DiscordConfiguration
{
    @NotNull
    private String token;

    public String getToken()
    {
        return token;
    }

    public void setToken( String token )
    {
        this.token = token;
    }
}
