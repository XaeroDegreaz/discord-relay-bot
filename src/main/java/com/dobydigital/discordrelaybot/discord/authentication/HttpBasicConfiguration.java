package com.dobydigital.discordrelaybot.discord.authentication;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import static com.dobydigital.discordrelaybot.discord.authentication.HttpBasicConfiguration.CONFIG_PATH;

@Configuration
@ConfigurationProperties( CONFIG_PATH )
@Validated
public class HttpBasicConfiguration
{
    public final static String CONFIG_PATH = "discord-configuration.api.authentication.http-basic";
    private String username;
    private String password;

    public String getUsername()
    {
        return username;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

}
