package com.dobydigital.discordrelaybot.discord.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import static java.util.Objects.nonNull;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private final static Logger log = LoggerFactory.getLogger( SecurityConfiguration.class );
    private final HttpBasicConfiguration httpBasicConfiguration;

    public SecurityConfiguration( HttpBasicConfiguration httpBasicConfiguration )
    {
        this.httpBasicConfiguration = httpBasicConfiguration;
    }

    @Override
    protected void configure( HttpSecurity http ) throws Exception
    {
        if ( nonNull( httpBasicConfiguration.getUsername() ) )
        {
            http.authorizeRequests()
                .anyRequest().fullyAuthenticated()
                .and().httpBasic()
                .and().csrf().disable();
        }
        else
        {
            http.authorizeRequests()
                .anyRequest().permitAll()
                .and().csrf().disable();
            log.warn( "configure() - The Discord Relay Bot currently has no authentication configured! " +
                      "It's recommended to set the username and password in the configuration under " +
                      "'" + HttpBasicConfiguration.CONFIG_PATH + "'." );
        }
    }

    @Autowired
    protected void configureGlobal( AuthenticationManagerBuilder auth ) throws Exception
    {
        if ( nonNull( httpBasicConfiguration.getUsername() ) )
        {
            auth.inMemoryAuthentication()
                .withUser( httpBasicConfiguration.getUsername() )
                .password( httpBasicConfiguration.getPassword() )
                .roles( "API_USER" );
        }
    }
}
