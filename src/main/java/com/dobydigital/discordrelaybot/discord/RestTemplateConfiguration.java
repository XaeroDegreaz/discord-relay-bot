package com.dobydigital.discordrelaybot.discord;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.client.AsyncRestTemplate;

@Configuration
public class RestTemplateConfiguration
{
    @Bean
    public AsyncRestTemplate asyncRestTemplate( ObjectMapper objectMapper )
    {
        AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
        //# The default AsyncRestTemplate does not seem to properly inject our Spring ObjectMapper configuration. We have to remove
        //# the old one, and add the correct one.
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter( objectMapper );
        asyncRestTemplate.getMessageConverters()
            .removeIf( MappingJackson2HttpMessageConverter.class::isInstance );
        asyncRestTemplate.getMessageConverters()
            .add( mappingJackson2HttpMessageConverter );
        //# We have to do something similar for the Xml converter. This one is a little more messy since we don't have
        //# an XmlMapper that we could autowire like we did with the JSON above. Instead, we simply modify the feature that
        //# we know causes the Java8 timestamps to get written funny.
        asyncRestTemplate.getMessageConverters()
            .stream()
            .filter( MappingJackson2XmlHttpMessageConverter.class::isInstance )
            .forEach( converter -> {
                MappingJackson2XmlHttpMessageConverter xmlConverter = (MappingJackson2XmlHttpMessageConverter) converter;
                XmlMapper xmlMapper = (XmlMapper) xmlConverter.getObjectMapper();
                xmlMapper.configure( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false );
            } );
        return asyncRestTemplate;
    }
}
