package com.dobydigital.discordrelaybot.discord;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.security.auth.login.LoginException;

@Configuration
public class JdaConfiguration
{
    private final static Logger log = LoggerFactory.getLogger( JdaConfiguration.class );

    @Bean
    public JDA jda( DiscordConfiguration discordConfiguration, DiscordMessageListener discordMessageListener )
    {
        try
        {
            return new JDABuilder( AccountType.BOT )
                .setToken( discordConfiguration.getToken() )
                .addEventListener( discordMessageListener )
                .buildAsync();
        }
        catch ( LoginException | RateLimitedException e )
        {
            String message = "There was an error constructing the JDA instance.";
            log.error( message, e );
            throw new RuntimeException( message, e );
        }
    }
}
