package com.dobydigital.discordrelaybot.discord;

import net.dv8tion.jda.core.entities.Message;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;

public class ApiMessage implements Serializable
{
    private static final long serialVersionUID = -4074673585117647434L;
    @NotNull( message = "'sender' may not be null." )
    private String sender;
    private Long senderId;
    @NotNull( message = "'message' may not be null." )
    private String message;
    @NotNull( message = "'channel' may not be null." )
    private String channel;
    private Long messageId;
    private OffsetDateTime createdAt;

    ApiMessage()
    {
    }

    ApiMessage( Message message )
    {
        this( message.getAuthor().getName(),
              message.getAuthor().getIdLong(),
              message.getContent(),
              message.getChannel().getName(),
              message.getIdLong(),
              message.getCreationTime() );
    }

    ApiMessage( String sender, Long senderId, String message, String channel, Long messageId, OffsetDateTime createdAt )
    {
        this.sender = sender;
        this.senderId = senderId;
        this.message = message;
        this.channel = channel;
        this.messageId = messageId;
        this.createdAt = createdAt;
    }

    public String getSender()
    {
        return sender;
    }

    public Long getSenderId()
    {
        return senderId;
    }

    public String getMessage()
    {
        return message;
    }

    public String getChannel()
    {
        return channel;
    }

    public Long getMessageId()
    {
        return messageId;
    }

    public OffsetDateTime getCreatedAt()
    {
        return createdAt;
    }
}
