package com.dobydigital.discordrelaybot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscordRelayBotApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run( DiscordRelayBotApplication.class, args );
    }
}
