package com.dobydigital.discordrelaybot.relay;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Configuration
@ConfigurationProperties( "external-api" )
@Validated
public class ExternalApiConfiguration
{
    @NotNull
    private String relayEndpoint;
    private Map<String, String> headers;
    private HttpHeaders httpHeaders;

    public String getRelayEndpoint()
    {
        return relayEndpoint;
    }

    public void setRelayEndpoint( String relayEndpoint )
    {
        this.relayEndpoint = relayEndpoint;
    }

    public Map<String, String> getHeaders()
    {
        return headers;
    }

    public void setHeaders( Map<String, String> headers )
    {
        this.headers = headers;
    }

    public HttpHeaders getHttpHeaders()
    {
        if ( httpHeaders == null && headers != null )
        {
            httpHeaders = new HttpHeaders();
            headers.forEach( httpHeaders::add );
        }
        return httpHeaders;
    }
}
